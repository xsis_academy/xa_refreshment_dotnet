﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BelajarDotNet._02.ModelData;
using BelajarDotNet._04.ModelView;

namespace BelajarDotNet._03.ModelAccess
{
    class ModelAccessAgama
    {
        public static List<ModelViewAgama> GetListAll(Int32 pPage, Int32 pPageSize, int pSort, string pKodeAgama)
        {
            List<ModelViewAgama> result = new List<ModelViewAgama>();

            using (var db = new DB_UniversitasEntities())
            {
                SqlParameter start = new SqlParameter("@start", pPage);
                SqlParameter display = new SqlParameter("@display", pPageSize);
                SqlParameter sorting = new SqlParameter("@sort", pSort);
                SqlParameter kodeAgama = new SqlParameter("@kode_agama", pKodeAgama);

                result = db.Database.SqlQuery<ModelViewAgama>("sp_agama_select @start, @display, @sort, @kode_agama", start, display, sorting, kodeAgama).ToList();
            }

            return result;
        }
        public static ModelViewAgama GetDetailsById(int pId)
        {
            ModelViewAgama result = new ModelViewAgama();

            using (var db = new DB_UniversitasEntities())
            {
                SqlParameter id = new SqlParameter("@id", pId);

                result = db.Database.SqlQuery<ModelViewAgama>("sp_agama_detail @id", id).FirstOrDefault();
            }

            return result;
        }
        public static ModelViewAgama Insert(ModelViewAgama pModel)
        {
            ModelViewAgama result = new ModelViewAgama();

            try
            {
                using (var db = new DB_UniversitasEntities())
                {
                    SqlParameter kodeAgama = new SqlParameter("@kode_agama", pModel.kode_agama);
                    SqlParameter description = new SqlParameter("@description", pModel.description);
                    SqlParameter isActive = new SqlParameter("@is_active", pModel.is_active);
                    SqlParameter createdBy = new SqlParameter("@user_by", pModel.created_by);

                    result = db.Database.SqlQuery<ModelViewAgama>("sp_agama_insert @kode_agama, @description, @is_active, @user_by", kodeAgama, description, isActive, createdBy).FirstOrDefault();
                }
            }
            catch (Exception hasError)
            {
                result.MESSAGE = hasError.Message.ToLower();
            }

            return result;
        }
        public static ModelViewAgama Update(ModelViewAgama pModel)
        {
            ModelViewAgama result = new ModelViewAgama();

            try
            {
                using (var db = new DB_UniversitasEntities())
                {
                    SqlParameter idAgama = new SqlParameter("@id_agama_pk", pModel.id_agama_pk);
                    SqlParameter kodeAgama = new SqlParameter("@kode_agama", pModel.kode_agama);
                    SqlParameter description = new SqlParameter("@description", pModel.description);
                    SqlParameter isActive = new SqlParameter("@is_active", pModel.is_active);
                    SqlParameter createdBy = new SqlParameter("@user_by", pModel.created_by);

                    result = db.Database.SqlQuery<ModelViewAgama>(
                        "sp_agama_update @id_agama_pk, @kode_agama, @description, @is_active, @user_by",
                        idAgama, kodeAgama, description, isActive, createdBy).FirstOrDefault();
                }
            }
            catch (Exception hasError)
            {
                result.MESSAGE = hasError.Message.ToLower();
            }

            return result;
        }
        public static ModelViewAgama Delete(ModelViewAgama pModel)
        {
            ModelViewAgama result = new ModelViewAgama();

            try
            {
                using (var db = new DB_UniversitasEntities())
                {
                    SqlParameter idAgama = new SqlParameter("@id_agama_pk", pModel.id_agama_pk);
                    SqlParameter isActive = new SqlParameter("@is_active", pModel.is_active);
                    SqlParameter createdBy = new SqlParameter("@user_by", pModel.created_by);

                    result = db.Database.SqlQuery<ModelViewAgama>(
                        "sp_agama_delete @id_agama_pk, @is_active, @user_by",
                        idAgama, isActive, createdBy).FirstOrDefault();
                }
            }
            catch (Exception hasError)
            {
                result.MESSAGE = hasError.Message.ToLower();
            }

            return result;
        }
        public static int GetCountData(string pKodeAgama)
        {
            int countData = 0;

            using (var db = new DB_UniversitasEntities())
            {
                SqlParameter kodeAgama = new SqlParameter("@kode_agama", pKodeAgama);

                countData = db.Database.SqlQuery<ModelViewAgama>("sp_agama_count_row @kode_agama", kodeAgama).Count();
            }

            return countData;
        }
    }
}
