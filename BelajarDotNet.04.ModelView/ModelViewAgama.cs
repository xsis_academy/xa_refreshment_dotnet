﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BelajarDotNet._04.ModelView
{
    public class ModelViewAgama
    {
        public int id_agama_pk { get; set; }
        [Required]
        [StringLength(5)]
        public string kode_agama { get; set; }
        [Required]
        [StringLength(5)]
        public string description { get; set; }
        [Column(TypeName = "Is Active")]
        public Nullable<bool> is_active { get; set; }
        public string created_by { get; set; }
        public System.DateTime created_date { get; set; }
        public string updated_by { get; set; }
        public Nullable<System.DateTime> updated_date { get; set; }

        #region -- status action create, update dan delete --
        public  bool STATUS { get; set; }
        public string MESSAGE { get; set; }
        #endregion
    }
}
